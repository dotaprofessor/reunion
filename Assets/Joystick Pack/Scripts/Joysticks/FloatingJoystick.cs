﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : Joystick
{
    private Vector2 basePosition;
    protected override void Start()
    {
        base.Start();
        // background.gameObject.SetActive(false);
        basePosition = background.anchoredPosition;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log(eventData.position);
        Debug.Log(ScreenPointToAnchoredPosition(eventData.position));
        background.anchoredPosition = eventData.position; //ScreenPointToAnchoredPosition(eventData.position);
        // background.gameObject.SetActive(true);
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        // background.gameObject.SetActive(false);
        background.anchoredPosition = basePosition;
        base.OnPointerUp(eventData);
    }
}