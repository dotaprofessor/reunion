﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData")]
public class GameData : ScriptableObject
{
    private static GameData instance;
    public static GameData Instance => GetInstance();

    private static GameData GetInstance()
    {
        if(instance == null)
        {
            return instance = Resources.Load<GameData>("Data/GameData");
        }
        return instance;
    }

    public Sprite switchBlueEnableSprite;
    public Sprite switchPinkEnableSprite;
    public Sprite switchBlueDisableSprite;
    public Sprite switchPinkDisableSprite;
}
