﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/MapData")]
public class MapData : ScriptableObject
{
    public List<MapDataInfo> mapDataInfo;
    private static MapData instance;
    public static MapData Instance => GetInstance();
    public static MapData GetInstance()
    {
        if(instance != null)
        {
            return instance;
        }
        return instance = Resources.Load<MapData>("Data/MapData");
    }
}
