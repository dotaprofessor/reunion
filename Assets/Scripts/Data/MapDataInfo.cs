﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Blue,
    Pink
}

[Serializable]
public class MapDataInfo
{
    public int id;
    public Vector2 blue;
    public Vector2 red;
    public int redKey;
    public int blueKey;
}