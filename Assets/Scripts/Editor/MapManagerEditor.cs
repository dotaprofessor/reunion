﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapManager))]
public class MapManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        MapManager myScript = (MapManager)target;

        EditorGUILayout.Space();
        if(GUILayout.Button("Save Map"))
        {
            myScript.SaveMapEditor();
        }
        if(GUILayout.Button("Load Map"))
        {
            myScript.LoadMapEditor();
        }
    }
}
