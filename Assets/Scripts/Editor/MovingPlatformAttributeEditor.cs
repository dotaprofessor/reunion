﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPlatformAttribute))]
public class MovingPlatformAttributeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        MovingPlatformAttribute myScript = (MovingPlatformAttribute)target;
        myScript.moveDirection = (MoveDirection)EditorGUILayout.EnumPopup("Moving Direction", myScript.moveDirection);

        if(myScript.moveDirection == MoveDirection.Horizontal)
        {
            myScript.startX = EditorGUILayout.FloatField("StartX", myScript.startX);
            myScript.endX = EditorGUILayout.FloatField("EndX", myScript.endX);
        }else if(myScript.moveDirection == MoveDirection.Vertical)
        {
            myScript.startY = EditorGUILayout.FloatField("StartY", myScript.startY);
            myScript.endY = EditorGUILayout.FloatField("EndY", myScript.endY);
        }
        myScript.timeToMove = EditorGUILayout.FloatField("Time To Move", myScript.timeToMove);
    }
}
