﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{
    public CameraFollow cameraFollow;
    private Transform playerTransform;

    public static GameManager instance;

    int currentLevel;
    public bool isBlue;
    public bool levelComplete = false;
    public bool isReloading = false;

    public int blueKeyRequired;
    public int pinkKeyRequired;

    public Transform completeWindow;
    public TextMeshProUGUI keyCountTxt;

    public FloatingJoystick joyStick;

    public static Action CharacterJump;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        MapManager.instance.Init();
        if (DataManager.instance != null)
        {
            OnStartGame(DataManager.instance.selectedLevel);
        }
        else
        {
            OnStartGame(MapManager.instance.mapID);
        }

        playerTransform = isBlue ? MapManager.instance.blueCharacter : MapManager.instance.redCharacter;
        cameraFollow.SetUp(() => playerTransform.position);
    }

    private void Update()
    {
//#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnChangeCharacter();
        }
//#endif
    }

    void OnStartGame(int level)
    {
        currentLevel = level;
        blueKeyRequired = 0;
        pinkKeyRequired = 0;
        MapManager.instance.OnStartGame(currentLevel);
        levelComplete = false;
    }

    public void OnChangeCharacter()
    {
        if (isBlue)
        {
            isBlue = false;
            cameraFollow.SetGetCameraFollowPositionFunc(() => MapManager.instance.redCharacter.transform.position);
        }
        else
        {
            isBlue = true;
            cameraFollow.SetGetCameraFollowPositionFunc(() => MapManager.instance.blueCharacter.transform.position);
        }
    }
    //public void Jump()
    //{
    //    if (isBlue)
    //    {
    //        MapManager.instance.blueCharacter.GetComponent<PlayerManager>().isJumping = true;
    //    }
    //    else
    //    {
    //        MapManager.instance.redCharacter.GetComponent<PlayerManager>().isJumping = true;
    //    }
    //}

    public void OnRestartLevel()
    {
        MapManager.instance.OnRestart();
    }

    public void CompleteLevel()
    {
        if (levelComplete)
        {
            return;
        }
        levelComplete = true;
        if (currentLevel >= DataManager.instance.completedLevel)
        {
            DataManager.instance.completedLevel++;
        }
        Utils.OnSaveGame();
    }

    public void OnNextLevel()
    {
        Utils.OnWindowFadeOut(completeWindow.GetChild(0), delegate
        {
            completeWindow.gameObject.SetActive(false);
            OnStartGame(++currentLevel);
        });
    }

    public void OnMainMenu()
    {
        //Utils.OnWindowFadeOut(completeWindow.GetChild(0), delegate
        //{
        //    SceneManager.LoadSceneAsync(0);
        //});
        SceneLoader.instance.LoadScene("MainMenu");
    }

    public void ShowCompleteWindow()
    {
        if (completeWindow == null)
        {
            return;
        }
        completeWindow.gameObject.SetActive(true);
        Utils.OnWindowFadeIn(completeWindow.GetChild(0));
    }

    public void UpdateKeyCount()
    {
        //keyCountTxt.text = "Have Blue Key: " + MapManager.instance.blueCharacter.GetComponent<PlayerManager>().keyCount;
        //keyCountTxt.text = "Have Red Key: " + MapManager.instance.redCharacter.GetComponent<PlayerManager>().keyCount;
    }

    public void OnJump()
    {
        CharacterJump?.Invoke();
    }

    public void OnSwitchCharacter()
    {
        OnChangeCharacter();
    }
}
