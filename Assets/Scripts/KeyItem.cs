﻿using DarkTonic.PoolBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : MonoBehaviour
{
    public ItemType type;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player_Blue" && type == ItemType.Blue)
        {
            collision.GetComponent<PlayerManager>().keyCount++;
            GameManager.instance.UpdateKeyCount();
            PoolBoss.Despawn(transform);
        }

        if(collision.tag == "Player_Pink" && type == ItemType.Pink)
        {
            collision.GetComponent<PlayerManager>().keyCount++;
            GameManager.instance.UpdateKeyCount();
            PoolBoss.Despawn(transform);
        }
    }
}