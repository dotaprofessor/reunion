﻿using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class LevelContainerManager : MonoBehaviour
{
    public Transform closeBtn;
    public Transform container;
    public Transform levelBtn;
    bool inited = false;

    public void InitData()
    {
        if (!inited)
        {
            for (int i = 0; i < MapData.Instance.mapDataInfo.Count; i++)
            {
                GameObject go = Instantiate(levelBtn.gameObject, Vector3.zero, Quaternion.identity, container);
                //go.transform.parent = container;
                //go.transform.SetParent(container);
                //go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;
                go.GetComponent<ButtonManager>().buttonText = (i + 1).ToString();
                int x = i;
                go.GetComponent<ButtonManager>().clickEvent.AddListener(() => OnLevelBtnClick(x));
                closeBtn.GetComponent<Button>().onClick.AddListener(delegate
                {
                    MainMenuManager.instance.levelContainer.CloseWindow();
                });
            }
            inited = true;
        }
    }

    void OnLevelBtnClick(int level)
    {
        DataManager.instance.selectedLevel = level;
        SceneLoader.instance.LoadScene("Gameplay");
    }

    public void OnFadeIn()
    {
        container.gameObject.SetActive(true);
        closeBtn.GetComponent<Button>().interactable = true;
    }

    public void OnFadeOut()
    {
        gameObject.SetActive(false);
    }
}
