﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager instance;
    public Button playBtn;
    public ModalWindowManager levelContainer;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        playBtn.onClick.AddListener(OnOpenLevelWindow);
    }

    private void OnOpenLevelWindow()
    {
        levelContainer.OpenWindow();
        levelContainer.GetComponent<LevelContainerManager>().InitData();
    }
}
