﻿using DarkTonic.PoolBoss;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public static MapManager instance;
    private MapData mapData;
    [SerializeField]
    public int mapID;

    [Header("Variable")]
    public Transform mapContainer;
    [HideInInspector]
    public Transform blueCharacter;
    [HideInInspector]
    public Transform redCharacter;
    public TextMeshProUGUI infoText;
    public int redKey;
    public int blueKey;

    [Header("Prefabs")]
    public Transform[] mapPrefabs;
    private Transform currentMap;

    private bool hasBlue;
    private bool hasRed;

    private void Awake()
    {
        instance = this;
    }

    public void Init()
    {
        PoolBoss.Initialize();
        mapData = MapData.Instance;
        blueCharacter = GameObject.FindGameObjectWithTag("Player_Blue").transform;
        redCharacter = GameObject.FindGameObjectWithTag("Player_Pink").transform;
    }

#if UNITY_EDITOR
    public void SaveMapEditor()
    {
        hasBlue = false;
        hasRed = false;
        if(mapData == null)
        {
            mapData = MapData.Instance;
        }

        // Check new map
        if (mapID == mapData.mapDataInfo.Count)
        {
            // Create new map
            MapDataInfo mapDataInfo = new MapDataInfo();
            mapDataInfo.id = mapID;
            foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>())
            {
                if (go.name == "CharacterBlue")
                {
                    mapDataInfo.blue = go.transform.position;
                    hasBlue = true;
                }
                if (go.name == "CharacterRed")
                {
                    mapDataInfo.red = go.transform.position;
                    hasRed = true;
                }
            }
            mapDataInfo.redKey = redKey;
            mapDataInfo.blueKey = blueKey;
            if (hasRed && hasBlue)
            {
                mapData.mapDataInfo.Add(mapDataInfo);
            }
            else
            {
                Debug.LogError("must have character blue, character red and end goal");
                return;
            }
        }
        else if (mapID > mapData.mapDataInfo.Count)
        {
            Debug.LogError("MapID is invalid\n" + "MapID must be less or equals than map data count\n" + "Current map data count = " + mapData.mapDataInfo.Count);
        }
        // if map is existed
        else
        {
            MapDataInfo mapDataInfo = new MapDataInfo();
            mapDataInfo.id = mapID;

            foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>())
            {
                if (go.name == "CharacterBlue")
                {
                    mapDataInfo.blue = go.transform.position;
                    hasBlue = true;
                }
                if (go.name == "CharacterRed")
                {
                    mapDataInfo.red = go.transform.position;
                    hasRed = true;
                }
            }

            mapDataInfo.redKey = redKey;
            mapDataInfo.blueKey = blueKey;

            if (hasRed && hasBlue)
            {
                mapData.mapDataInfo[mapID] = mapDataInfo;
            }
            else
            {
                Debug.LogError("must have character blue, character red and end goal");
                return;
            }
        }
        if (infoText != null)
        {
            infoText.text = "Cached MapID: " + mapID;
        }
        EditorUtility.SetDirty(mapData);
    }

    public void LoadMapEditor()
    {
        if (mapData == null)
        {
            mapData = MapData.Instance;
        }

        // if mapID is out of range
        if (mapID + 1 > mapData.mapDataInfo.Count)
        {
            Debug.LogError("Map ID is not exist in map data\n" + "MapID must be less than map data count\n" + "Current map data count = " + mapData.mapDataInfo.Count);
            return;
        }

        if (currentMap != null)
        {
            OnClearCachedObject(currentMap);
        }

        // create new data
        // create player data
        blueCharacter = GameObject.FindGameObjectWithTag("Player_Blue").transform;
        blueCharacter.transform.position = mapData.mapDataInfo[mapID].blue;
        redCharacter = GameObject.FindGameObjectWithTag("Player_Pink").transform;
        redCharacter.transform.position = mapData.mapDataInfo[mapID].red;

        redKey = mapData.mapDataInfo[mapID].redKey;
        blueKey = mapData.mapDataInfo[mapID].blueKey;

        if (mapContainer.childCount == 0)
        {
            GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(mapPrefabs[mapID].gameObject);
            go.transform.parent = mapContainer;
            go.transform.position = Vector3.zero;
            currentMap = go.transform;
        }
        else
        {
            currentMap = mapContainer.GetChild(0);
        }
        if (infoText != null)
        {
            infoText.text = "Cached MapID: " + mapID;
        }
    }

    void OnClearCachedObject(Transform cMap)
    {
        DestroyImmediate(cMap.gameObject);
    }
#endif
        

    public void OnStartGame(int level)
    {
        LoadData(level);
    }

    void OnClearMapCachedObject(Transform cMap)
    {
        PoolBoss.Despawn(cMap.transform);
    }

    void LoadData(int level)
    {
        mapID = level;

        if (currentMap != null)
        {
            // Clear cached object
            OnClearMapCachedObject(currentMap);
        }
        if (DataManager.instance != null)
        {
            Transform trans = PoolBoss.Spawn(mapPrefabs[mapID], Vector3.zero, Quaternion.identity, mapContainer);
            trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            currentMap = trans;
        }
        blueCharacter.transform.position = mapData.mapDataInfo[mapID].blue;
        redCharacter.transform.position = mapData.mapDataInfo[mapID].red;

        GameManager.instance.blueKeyRequired = mapData.mapDataInfo[mapID].blueKey;
        GameManager.instance.pinkKeyRequired = mapData.mapDataInfo[mapID].redKey;
    }

    public void OnRestart()
    {
        blueCharacter.position = mapData.mapDataInfo[mapID].blue;
        redCharacter.position = mapData.mapDataInfo[mapID].red;
        Platform[] platform = FindObjectsOfType<Platform>();
        for (int i = 0; i < platform.Length; i++)
        {
            platform[i].UnColor();
        }
    }
}