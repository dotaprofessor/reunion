﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection
{
    Horizontal,
    Vertical
}

public class MovingPlatformAttribute : MonoBehaviour
{
    public MoveDirection moveDirection;
    public float startX;
    public float endX;
    public float startY;
    public float endY;
    public float timeToMove;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private Vector3 targetPosition;

    private void Start()
    {
        InitPlatform();
    }

    void InitPlatform()
    {
        if (moveDirection == MoveDirection.Horizontal)
        {
            startPosition = new Vector3(startX, transform.position.y, transform.position.z);
            endPosition = new Vector3(endX, transform.position.y, transform.position.z);
        }
        else if (moveDirection == MoveDirection.Vertical)
        {
            startPosition = new Vector3(transform.position.x, startY, transform.position.z);
            endPosition = new Vector3(transform.position.x, endY, transform.position.z);
        }
        targetPosition = endPosition;
        Move();
    }

    private void Move()
    {
        if (Vector3.Distance(transform.position, targetPosition) <= 1)
        {
            if (targetPosition == endPosition)
            {
                targetPosition = startPosition;
            }
            else
            {
                targetPosition = endPosition;
            }
        }
        transform.DOMove(targetPosition, timeToMove).onComplete += delegate
        {
            Move();
        };
    }

    //private void Update()
    //{
    //    float step = speed + Time.deltaTime;
    //    if (Vector3.Distance(transform.position, targetPosition) <= 1)
    //    {
    //        if (targetPosition == endPosition)
    //        {
    //            targetPosition = startPosition;
    //        }
    //        else
    //        {
    //            targetPosition = endPosition;
    //        }
    //    }
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
    //}
}