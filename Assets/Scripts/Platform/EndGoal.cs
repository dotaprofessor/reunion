﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGoal : MonoBehaviour
{
	public bool hasBlue;
	public bool hasRed;

	private void OnTriggerExit2D(Collider2D col)
	{
		if (col.CompareTag("Player_BottomTrigger"))
		{
			string parentTag = col.transform.parent.tag;
			if (parentTag == "Player_Blue")
			{
				hasBlue = false;
			}
			else if (parentTag == "Player_Pink")
			{
				hasRed = false;
			}
		}
	}

	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.CompareTag("Player_BottomTrigger"))
		{
			string parentTag = col.transform.parent.tag;
			if (parentTag == "Player_Blue")
			{
				hasBlue = true;
			}
			else if (parentTag == "Player_Pink")
			{
				hasRed = true;
			}
		}

		if(hasRed && hasBlue)
        {
			if(GameManager.instance.blueKeyRequired > 0 && GameManager.instance.pinkKeyRequired > 0)
            {
				if(MapManager.instance.blueCharacter.GetComponent<PlayerManager>().keyCount == GameManager.instance.blueKeyRequired && MapManager.instance.redCharacter.GetComponent<PlayerManager>().keyCount == GameManager.instance.pinkKeyRequired)
                {
					GameManager.instance.CompleteLevel();
                }
            }
            else
            {
				GameManager.instance.CompleteLevel();
            }
        }
	}
}
