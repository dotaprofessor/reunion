﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public int platformID;
    [Tooltip("1 = blue, 2 = red")]
    public int colorID;

    public Sprite blue;
    public Sprite red;

    public GameObject blueLight;
    public GameObject redLight;

    public LayerMask blueMask;
    public LayerMask redMask;

    public bool isStandable = true;
    public bool isOutline = false;

    public bool makeBlue;
    public bool makeRed;

    private SpriteRenderer platformSpr;
    PlatformEffector2D effector;

    private Sprite startSprite;
    private LayerMask startMask;

    private void Start()
    {
        platformSpr = GetComponent<SpriteRenderer>();
        effector = GetComponent<PlatformEffector2D>();
        startSprite = platformSpr.sprite;
        if (effector != null)
        {
            startMask = effector.colliderMask;
        }

        if (makeBlue)
        {
            MakeBlue();
        }
        if (makeRed)
        {
            MakeRed();
        }
    }

    public void MakeBlue()
    {
        platformSpr.sprite = blue;
        platformSpr.color = new Color(1, 1, 1, 1);
        //redLight.SetActive(false);
        //blueLight.SetActive(true);
        gameObject.layer = LayerMask.NameToLayer("Blue");
        if (effector != null)
        {
            effector.colliderMask = blueMask;
        }
    }

    public void MakeRed()
    {
        platformSpr.sprite = red;
        platformSpr.color = new Color(1, 1, 1, 1);
        //redLight.SetActive(true);
        //blueLight.SetActive(false);
        gameObject.layer = LayerMask.NameToLayer("Pink");
        if (effector != null)
        {
            effector.colliderMask = redMask;
        }
    }

    public void UnColor()
    {
        platformSpr.sprite = startSprite;
        redLight.gameObject.SetActive(false);
        blueLight.gameObject.SetActive(false);
        if (isOutline)
        {
            gameObject.layer = LayerMask.NameToLayer("Outline");
            platformSpr.color = new Color(1, 1, 1, 0.7f);
        }
        else
        {
            gameObject.layer = LayerMask.NameToLayer("Platform");
        }
        if (effector != null)
        {
            effector.colliderMask = startMask;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (isOutline)
        {
            return;
        }

        if (isStandable)
        {
            if (col.CompareTag("Player_BottomTrigger"))
            {
                string parentTag = col.transform.parent.tag;
                if (parentTag == "Player_Blue")
                {
                    MakeBlue();
                }
                else if (parentTag == "Player_Pink")
                {
                    MakeRed();
                }
            }
        }
        else
        {
            if (col.CompareTag("Player_Blue"))
            {
                MakeBlue();
            }
            else if (col.CompareTag("Player_Pink"))
            {
                 MakeRed();
            }
        }
    }

    void OnDespawned()
    {
        UnColor();
    }
}
