﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalPlatform : MonoBehaviour
{
    public PortalSpawnPoint spawnPoint;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player_Blue" || collision.tag == "Player_Pink")
        {
            collision.GetComponent<PlayerManager>().Teleport(spawnPoint.transform.position);
        }
    }
}
