﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwitchPlatform : MonoBehaviour
{
	public int platformID;

    public bool isPressed = false;
	public Animator anim;

    public UnityEvent OnPressed;
    public UnityEvent OnUnPressed;

    public bool isBlue;
    public bool isRed;

	public List<Platform> outlinePlatform;
	public SpriteRenderer switchColorSprite;

    private void Start()
    {
		Init();
    }

    public void Init()
    {
		outlinePlatform = new List<Platform>();
		foreach (Platform platform in FindObjectsOfType<Platform>())
		{
			if (platform.isOutline)
			{
				if (platformID == platform.platformID)
				{
					outlinePlatform.Add(platform);
				}
			}
		}

		//// blue
		//if (outlinePlatform[0].colorID == 1)
		//{
		//	switchColorSprite.sprite = GameData.Instance.switchBlueDisableSprite;
		//}
		//// red
		//else if (outlinePlatform[0].colorID == 2)
		//{
		//	switchColorSprite.sprite = GameData.Instance.switchPinkDisableSprite;
		//}
	}

    void Press()
    {
        isPressed = true;
		anim.SetBool("IsEnabled", true);
		OnPressChangeColor();
		OnPressed?.Invoke();
    }

    void UnPress()
    {
        isPressed = false;
		anim.SetBool("IsEnabled", false);
		OnUnpressChangeColor();
        OnUnPressed?.Invoke();
    }

	void OnPressChangeColor()
    {
		// blue
		if (outlinePlatform[0].colorID == 1)
		{
			outlinePlatform[0].MakeBlue();
		}
		// red
		else if (outlinePlatform[0].colorID == 2)
		{
			outlinePlatform[0].MakeRed();
		}
    }

	void OnUnpressChangeColor()
    {
		foreach (Platform platform in FindObjectsOfType<Platform>())
		{
			if (platform.isOutline)
			{
				if (platformID == platform.platformID)
				{
					platform.UnColor();
				}
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("Player_BottomTrigger"))
		{
			if (col.transform.parent.GetComponent<Rigidbody2D>().velocity.y <= 0f)
			{
				Press();
			}

			string parentTag = col.transform.parent.tag;
			if (parentTag == "Player_Blue")
			{
				isBlue = true;
			}
			else if (parentTag == "Player_Pink")
			{
				isRed = true;
			}
		}
	}

	private void OnTriggerExit2D(Collider2D col)
	{
		if (col.CompareTag("Player_BottomTrigger"))
		{
			string parentTag = col.transform.parent.tag;
			if (parentTag == "Player_Blue")
			{
				isBlue = false;
			}
			else if (parentTag == "Player_Pink")
			{
				isRed = false;
			}

			if (!isBlue && !isRed)
				UnPress();
		}
	}
}
