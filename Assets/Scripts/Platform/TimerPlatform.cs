﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerPlatform : MonoBehaviour
{
	public float changeInterval = 1f;
	public bool isBlue = true;

	public Sprite blue;
	public Sprite red;

	public GameObject blueLight;
	public GameObject redLight;

	public LayerMask blueMask;
	public LayerMask redMask;

	SpriteRenderer sr;
	PlatformEffector2D effector;

	float timeToChange;

	private void Start()
	{
		sr = GetComponent<SpriteRenderer>();
		effector = GetComponent<PlatformEffector2D>();

		timeToChange = changeInterval;

		if (isBlue)
			MakeBlue();
		else
			MakeRed();
	}

	private void Update()
	{
		if (Time.time >= timeToChange)
		{
			if (isBlue)
				MakeRed();
			else
				MakeBlue();

			timeToChange = Time.time + changeInterval;
		}
	}

	void MakeBlue()
	{
		sr.sprite = blue;
		//redLight.SetActive(false);
		//blueLight.SetActive(true);
		gameObject.layer = LayerMask.NameToLayer("Blue");
		if (effector != null)
			effector.colliderMask = blueMask;
		isBlue = true;
	}

	void MakeRed()
	{
		sr.sprite = red;
		//redLight.SetActive(true);
		//blueLight.SetActive(false);
		gameObject.layer = LayerMask.NameToLayer("Pink");
		if (effector != null)
			effector.colliderMask = redMask;
		isBlue = false;
	}
}
