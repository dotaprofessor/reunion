﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnd : MonoBehaviour
{
	public PlayerManager movement;
	public GameObject heart;

	private bool hasEnded = false;

	// Update is called once per frame
	void Update()
	{
		if (GameManager.instance.levelComplete && !hasEnded && !GameManager.instance.completeWindow.gameObject.activeSelf)
		{
			hasEnded = true;
			movement.enabled = false;
			if (heart != null)
			{
				heart.SetActive(true);
			}
			GameManager.instance.ShowCompleteWindow();
        }
        else
        {
			hasEnded = false;
			movement.enabled = true;
            if (heart != null)
            {
				heart.SetActive(false);
            }
        }
	}
}
