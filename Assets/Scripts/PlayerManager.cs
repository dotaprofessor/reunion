﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public bool isBlue;

    public float speed;
    public float maxSpeed;

    public float jumpSpeed;
    public float horizontalDrag;

    public Vector2 groundCheckPosition;
    public float checkRadius;
    public LayerMask whatIsGround;

    private Rigidbody2D rb;

    private float movement;
    public bool isJumping;

    private bool isGround;
    private bool isFlipped;

    public int keyCount;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        GameManager.CharacterJump += Jump;
    }

    public void Jump()
    {
        if (!isGround)
        {
            return;
        }
        isJumping = true;
    }

    private void Update()
    {
        if (GameManager.instance.levelComplete)
        {
            return;
        }

        if (GameManager.instance.isBlue)
        {
            if (!isBlue)
            {
                return;
            }
        }
        else
        {
            if (isBlue)
            {
                return;
            }
        }

#if UNITY_EDITOR
        movement = Input.GetAxisRaw("Horizontal") * speed;

        if (Input.GetKeyDown(KeyCode.W) && isGround)
        {
            isJumping = true;
        }

#elif UNITY_ANDROID || UNITY_IOS
        movement = GameManager.instance.joyStick.Horizontal * speed;
#endif


        //#if UNITY_EDITOR
        //#endif

        Collider2D collider = Physics2D.OverlapCircle(rb.position + groundCheckPosition, checkRadius, whatIsGround);
        if (collider != null)
        {
            if (rb.velocity.y <= 0)
            {
                isGround = true;
            }
        }
        else
        {
            isGround = false;
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.instance.levelComplete)
        {
            return;
        }

        if (GameManager.instance.isBlue)
        {
            if (!isBlue)
            {
                return;
            }
        }
        else
        {
            if (isBlue)
            {
                return;
            }
        }

        float t = rb.velocity.x / maxSpeed;
        float lerp = 0;
        if (t >= 0)
        {
            lerp = Mathf.Lerp(maxSpeed, 0, t);
        }
        else if (t < 0)
        {
            lerp = Mathf.Lerp(maxSpeed, 0, Mathf.Abs(t));
        }
        
        Vector2 force = new Vector2(movement * lerp * speed * Time.fixedDeltaTime, 0);
        Vector2 drag = new Vector2(-rb.velocity.x * horizontalDrag * Time.fixedDeltaTime, 0);

        rb.AddForce(force, ForceMode2D.Force);
        rb.AddForce(drag, ForceMode2D.Force);

        Vector2 flipScale = new Vector2(-1, 1);

        if (movement >= 0.1f && isFlipped)
        {
            transform.localScale *= flipScale;
            isFlipped = false;
        }
        else if (movement <= -0.1f && !isFlipped)
        {
            transform.localScale *= flipScale;
            isFlipped = true;
        }

        if (isJumping)
        {
            Vector2 velocity = rb.velocity;
            velocity.y = jumpSpeed;
            rb.velocity = velocity;
            isJumping = false;
            isGround = false;
        }
    }

    public void Teleport(Vector2 destination)
    {
        transform.position = destination;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere((Vector2)transform.position + groundCheckPosition, checkRadius);
    }

    private void OnDisable()
    {
        
    }
}
