﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveScript : MonoBehaviour
{
    public static SaveScript instance;
    string savePath;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        savePath = Application.persistentDataPath + "/gamesave.dat";
        LoadGame();
    }

    public void SaveGame()
    {
        var saveData = new SaveData()
        {
            completedLevel = DataManager.instance.completedLevel
        };
        var binaryFommater = new BinaryFormatter();
        using (var fileStream = File.Create(savePath))
        {
            binaryFommater.Serialize(fileStream, saveData);
        }
        Debug.LogError("Game Saved");
    }

    public void LoadGame()
    {
        Debug.LogError(Application.persistentDataPath);
        if (File.Exists(savePath))
        {
            SaveData saveData = new SaveData();

            var binaryFommater = new BinaryFormatter();
            using (var fileStream = File.Open(savePath, FileMode.Open))
            {
                saveData = (SaveData)binaryFommater.Deserialize(fileStream);
            }

            DataManager.instance.completedLevel = saveData.completedLevel;
        }
        else
        {
            Debug.LogError("No Save Found");
            return;
        }
        Debug.LogError("Game Loaded");
    }
}
