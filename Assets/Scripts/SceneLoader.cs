﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.UI.ModernUIPack;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public GameObject loadingScreen;
    public ProgressBar progressBar;
    private CanvasGroup canvasGroup;
    public static SceneLoader instance;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(StartLoad(sceneName));
        progressBar.currentPercent = 0;
    }

    IEnumerator StartLoad(string sceneName)
    {
        loadingScreen.gameObject.SetActive(true);
        yield return StartCoroutine(FadeLoadingScreen(1, 1));

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        while (!operation.isDone)
        {
            float progressValue = Mathf.Clamp01(operation.progress / 0.9f);
            progressBar.currentPercent = progressValue * 100;
            yield return null;
        }

        yield return StartCoroutine(FadeLoadingScreen(0,1));
        loadingScreen.SetActive(false);
    }

    IEnumerator FadeLoadingScreen(float targetValue, float duration)
    {
        float startvalue = canvasGroup.alpha;
        float time = 0;

        while(time < duration)
        {
            canvasGroup.alpha = Mathf.Lerp(startvalue, targetValue, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        canvasGroup.alpha = targetValue;
    }
}
