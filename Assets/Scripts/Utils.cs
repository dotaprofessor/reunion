﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Utils
{
    public delegate void OnFadeComplete();
    public OnFadeComplete onFadeComplete;

    public static void OnWindowFadeIn(Transform window, OnFadeComplete callback = null)
    {
        if (callback == null)
        {
            window.DOScale(Vector3.one, 0.2f);
        }
        else
        {
            window.DOScale(Vector3.one, 0.2f).onComplete += delegate
            {
                callback();
            };
        }
    }

    public static void OnWindowFadeOut(Transform window, OnFadeComplete callback = null)
    {
        if (callback == null)
        {
            window.DOScale(new Vector3(0.3f, 0.3f), 0.2f);
        }
        else
        {
            window.DOScale(new Vector3(0.3f, 0.3f), 0.2f).onComplete += delegate
            {
                callback();
            };
        }
    }

    public static void OnSaveGame()
    {
        SaveScript.instance.SaveGame();
    }
}
